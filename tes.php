<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<!--     <link rel="icon" href="../../favicon.ico"> -->

    <title>Data Lama|Web Monitoring</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="assets/css/justified-nav.css" rel="stylesheet">

    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    
  </head>

  <body>

    <div class="container" style="background-color: white; border: 0.5px solid grey;">


      <!-- Buat Header -->
      <div class="row">
        <div  style="padding: 15px;">
          <img src="assets/img/header2.png" width="100%">
        </div>
      </div>
      <!-- Akhir Header -->

      <!-- Buat Tombol Menu -->
      <div class="masthead" >
        <nav>
          <ul class="nav nav-justified" >
            <li><a href="beranda.php">Beranda</a></li>
            <li class="active"><a href="data_lama.php">Data Lama</a></li>
            <li><a href="index.php">Keluar</a></li>
          </ul>
        </nav>
      </div>
      <!-- Akhir Tombol Menu -->
      <!-- Buat Content -->
            <div class="row">

        <div class="col-lg-5">
        
        <br>

          <div class="panel panel-default" style="height: ; background-color:#ECF0F1; box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2), 0 1.5px 5px 0 rgba(0, 0, 0, 0.19);">
            <!-- <div class="panel-heading">Selamat Datang, Admin</div> -->
            <div class="panel-body">

              <form class="form-horizontal" style="background-color: ;">
                <script type="text/javascript">
		        $(function () {
			        var chart;
		            	$(document).ready(function() {
				        $.getJSON("dataline.php", function(json) {
				
					    chart = new Highcharts.Chart({
						chart: {
							renderTo: 'mygraph',
							type: 'line'
							
						},
						title: {
							text: 'Grafik Data yang Tersimpan'
							
						},
						subtitle: {
							text: '(Grafik Data Suhu dan PWM)'
						
						},
						xAxis: {
							categories: ['2017-01-01', '2017-02-01', '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01', '2017-07-01', '2017-08-01', '2017-09-01', '2017-10-01', '2017-11-01', '2017-12-01']
						},
						yAxis: {
							title: {
								text: 'Data Keluaran Suhu dan PWM'
							},
							plotLines: [{
								value: 0,
								width: 1,
								color: '#808080'
							}]
						},
						tooltip: {
							formatter: function() {
									return '<b>'+ this.series.name +'</b><br/>'+
									this.x +': '+ this.y;
							}
						},
						legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'top',
							x: -10,
							y: 120,
							borderWidth: 0
						},
						series: json
					});
				});
			
			});
			
		});
		</script>
	<script src="assets/js/highcharts.js"></script>
	<script src="assets/js/exporting.js"></script>
        
              </form>

            </div>
          </div>

        </div>

        <div class="col-lg-7">
    
          <br>

          <div class="panel panel-default" style="height: ; background-color:#ECF0F1; box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2), 0 1.5px 5px 0 rgba(0, 0, 0, 0.19);">
            <!-- <div class="panel-heading">Denah Kelas</div> -->
            <div class="panel-body">
              <h2 class="text-center text-danger">Tabel Data Tersimpan</h2>
              <?php
                //untuk koneksi database
                include "koneksidalum.php";
                //untuk menantukan tanggal awal dan tanggal akhir data di database
                $min_tanggal=mysql_fetch_array(mysql_query("select min(tgl) as min_tgl from tabel_dalum"));
                $max_tanggal=mysql_fetch_array(mysql_query("select max(tgl) as max_tgl from tabel_dalum"));
              ?>

                <div class="page-header">
                    <center>
                        <form action="data_lama.php" method="post" name="postform">

                        <table width="550" border="0">
                            <td width="70">Tanggal Awal</td>
                            <td colspan="2"><input type="text" name="tgl_awal" size="10" placeholder="yyyy-mm-dd"/>
                                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.postform.tgl_awal);return false;" ><img src="calender/calender.jpeg" alt="" name="popcal" width="34" height="29" border="0" align="absmiddle" id="popcal" /></a>				
                            </td><td width="70">Tanggal Akhir</td> <td colspan="2"><input type="text" name="tgl_akhir" size="10" placeholder="yyyy-mm-dd"/>
                                <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.postform.tgl_akhir);return false;" ><img src="calender/calender.jpeg" alt="" name="popcal" width="34" height="29" border="0" align="absmiddle" id="popcal" /></a>				
                            <td width="50">
                            <button type="submit" name="cari" class="btn btn-white btn-info btn-bold">Tampilkan Data</button>
                            </td></tr>
                        </table>
                        </form></center><br/>
                </div>

                <?php
                //di proses jika sudah klik tombol cari
                if(isset($_POST['cari'])){
	
	            //menangkap nilai form
	                $tgl_awal=$_POST['tgl_awal'];
	                $tgl_akhir=$_POST['tgl_akhir'];
	            if (empty($tgl_awal) and empty($tgl_akhir)){
		        //jika tidak menginput apa2
		            $query=mysql_query("select * from tabel_dalum");
		
	            }else{
		
		        ?>
                    <div class="row">
	                    <div class="col-xs-12">
		                    <div class="row">
			                    <div class="col-xs-12">
                                    <div class="table-header"><i><b>Informasi Data: </b> dari tanggal <b><?php echo $_POST['tgl_awal']?></b> sampai dengan tanggal <b><?php echo $_POST['tgl_akhir']?></b></i></div><?php
		
            		$query=mysql_query("select * from tabel_dalum where tgl between '$tgl_awal' and '$tgl_akhir'");
		
	               }
	
	            ?>

                <table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Tanggal</th>
								<th>Suhu</th>
								<th>PWM</th>
							</tr>
						</thead>
                <?php
	            //untuk penomoran data
	                $no=0;
	
            	//menampilkan data
	                while($row=mysql_fetch_array($query)){
	            ?>
                <tr>
                    <td><?php echo $row['tgl'];?></td> 
                    <td><?php echo $row['suhu'];?></td>                            
                    <td><?php echo $row['pwm'];?></td><td align="right">
                </tr>
                <?php
	            }
	            ?>

     
                </table>
                </div>
            </div>
        </div>
    </div>

        <?php
            }else{
	            unset($_POST['cari']);
                    }
        ?>

            </div>
          </div>

        </div>

      </div>
      <!-- Akhir Content -->

      <!-- Site footer -->
      <footer class="footer text-center">
        <p>&copy; 2016 webmonitoring.club, Ivan Fadillah Achmad. Pembimbing 1 : Unang Sunarya, Pembimbing 2 : Dwi Andi Nurmantris.</p>
      </footer>

      <br>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
