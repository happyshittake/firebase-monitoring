import * as admin from "firebase-admin";
import Sequelize from "sequelize";
import Express from "express";
import path from "path";
import moment from "moment";
import bodyParser from "body-parser";
import session from "express-session";
import passport from "passport";
import cookieParser from "cookie-parser";
import { Strategy } from "passport-local";
import {ensureLoggedIn} from "connect-ensure-login";

let server = Express();

let serviceAccount = require("../serviceaccount.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://kipas-mandiri.firebaseio.com/"
});

let sequelize = new Sequelize("monitoring", "root", "sempak", {
  host: "localhost",
  dialect: "mysql"
});

let Monitoring = sequelize.define("monitoring", {
  kipas: {
    type: Sequelize.INTEGER
  },
  kontrol: {
    type: Sequelize.INTEGER
  },
  pwm: {
    type: Sequelize.INTEGER
  },
  suhu: {
    type: Sequelize.INTEGER
  }
});

sequelize.sync();
passport.use(
  new Strategy(function(username, password, cb) {
    if (username != "admin" || password != "admin") {
      return cb(null, false);
    }

    return cb(null, "admin");
  })
);

passport.serializeUser((username, cb) => {
  cb(null, username);
});
passport.deserializeUser((username, cb) => {
  return cb(null, username);
});

server.set("views", path.join(__dirname, "/views"));
server.use("/assets", Express.static(path.join(__dirname, "html/assets")));
server.set("view engine", "pug");
server.use(
  session({ secret: "blablabla", resave: true, saveUninitialized: true })
);
server.use(bodyParser.urlencoded({ extended: true }));
server.use(cookieParser());
server.use(passport.initialize());
server.use(passport.session());
function startListeners() {
  let dbRef = admin.database().ref("/");
  dbRef.on("value", handler);
  console.log("listening firebase value event");
}

function handler(snapshot, prevChildKey) {
  let data = snapshot.exportVal();
  console.log(data);

  Monitoring.create({
    kipas: data.kipas,
    kontrol: data.kontrol.kontrol,
    pwm: data.pwm,
    suhu: data.suhu
  });
}

server.get("/", ensureLoggedIn(), (req, res) => {
  let pageTitle = "Beranda";
  res.render("beranda", { pageTitle });
});

server.get("/login", (req, res) => {
  res.render("login");
});

server.post(
  "/login",
  passport.authenticate("local", { failureRedirect: "/login" }),
  (req, res) => {
    res.redirect("/");
  }
);

server.get("/logout", ensureLoggedIn(), (req, res) => {
  req.logout();
  res.redirect("/login");
});

server.get("/history", ensureLoggedIn(), async (req, res) => {
  let items;
  let tglAtas;
  let tglBawah;
  let pageTitle = "History Monitoring";
  if (!req.query.tgl_awal || !req.query.tgl_akhir) {
    items = [];
    tglBawah = moment().format("YYYY-MM-DD");
    tglAtas = moment().add(1, "d").format("YYYY-MM-DD");
    res.render("history", { items, tglBawah, tglAtas, pageTitle });
  }

  tglBawah = moment(req.query.tgl_awal, "YYYY-MM-DD").format();
  tglAtas = moment(req.query.tgl_akhir, "YYYY-MM-DD").format();
  items = await Monitoring.findAll({
    where: {
      createdAt: {
        $lt: tglAtas,
        $gt: tglBawah
      }
    },
    sort: "id DESC"
  });
  res.render("history", {
    items,
    tglBawah: moment(tglBawah).format("YYYY-MM-DD"),
    tglAtas: moment(tglAtas).format("YYYY-MM-DD"),
    pageTitle
  });
});

server.listen(3000, () => {
  console.log("server listening at port 3000");
});

startListeners();
